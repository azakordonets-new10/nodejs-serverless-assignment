# Node.js Assignment (Serverless)

> We are going to (re)write a simple loan application, that will be split in two apps and communicate with each other through a REST API. The main goal of this assignment is to test your skills understanding, implementing new functionality, refactoring and writing tests. The current code is full of bad practices, outdated libraries and inconsistencies, and it's your goal to make it shine and up-to-date, keeping it simple enough.

### Requirements

- Node >= v14.x.x
- Serverless.com CLI
- Yarn (optional)
- Docker & Docker-compose (Required for running DynamoDB locally)
- API key for [openkvk](https://overheid.io/documentatie/openkvk)

### Getting started

- Run Dynamodb locally
```
> cd ./dynamo-db-local-docker
> docker-compose up

- Now you should have dynamodb-local and dynamodb-gui containers running locally.
- dynamodb-gui - GUI for dynamodb (http://0.0.0.0:8001) 
```

- Set up environment variables
```
> cd ./config
> cp ./env.sample.yml env.dev.yml

- Add your OpenKvK API key to OPENKVK_API_KEY variable
```

- Install dependencies: `yarn install`
- Run for development: `yarn start`
- Run tests: `yarn test`
- Check lint issues: `yarn lint`
- Type check: `yarn type-check`

### Technologies

- Platform: Node.js
- Programming language: Typescript / Javascript (ES6)
- Framework: Serverless.com
- Main AWS Services: Lambda, DynamoDB, SQS

## The assignment

- Task 1: redesign the API and implement proper validations on inputs, with proper error messages and status code. ✅
- Task 2: extend the create loan endpoint to also receive the `id` of the company on [openkvk](https://overheid.io/documentatie/openkvk). Only `active` companies should be allowed and you should store all information about the company on DynamoDB. ✅
- Task 3: implement disburse functionality ✅

### Improvements

- Configure SQS to dispatch events in batches. Currently, it's configured to send one event at a time.
- Add more integration test.
- Investigate generated bundle size.
- Install git hooks via [Husky](https://github.com/typicode/husky) to lint commit messages, run linting, and test.
- CI Setup
- API Documentation
- Use [pino-pretty](https://github.com/pinojs/pino-pretty) to pretty print logs in development.

### Issues

- The `serverless dynamodb install` command didn't work due to some dependency issues. So, `dynamodb-local` has been configured through Docker.
- Unable to set up `SQS` locally. So, disburse function fails when trying to add a job to the queue.
