import { SQSClient, SendMessageCommand, SendMessageCommandInput } from '@aws-sdk/client-sqs';

// development issue: Currently, Lift plugin doesn't work with serverless-offline.
// See https://github.com/getlift/lift/issues/106
const queueUrl = process.env.QUEUE_URL;

const client = new SQSClient({});

export async function sendToQueue(message: string) {
    const params: SendMessageCommandInput = {
        QueueUrl: queueUrl,
        MessageBody: message,
    };

    await client.send(new SendMessageCommand(params));
}
