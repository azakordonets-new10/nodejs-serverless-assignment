import { DynamoDBClient, DynamoDBClientConfig } from '@aws-sdk/client-dynamodb';

let options: DynamoDBClientConfig = {};

// connect to local DB if running offline
if (process.env.IS_OFFLINE) {
    options = {
        region: 'localhost',
        endpoint: 'http://localhost:8000',
    };
}

export const DBClient = new DynamoDBClient(options);
