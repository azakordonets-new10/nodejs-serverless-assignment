export enum ResponseCodes {
    ok = 200,
    created = 201,
    accepted = 202,
    badRequest = 400,
    notFound = 404,
    unprocessable = 422,
    error = 500,
}
