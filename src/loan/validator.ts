import Joi, { Schema } from 'joi';

export const applyLoanSchema = Joi.object({
    amount: Joi.number().positive().required(),
    companyId: Joi.string().required(),
});

interface ValidationResult<Data = unknown> {
    value: undefined | Data;
    errors: null | { message: string }[];
}

export function validate<Data = unknown>(schema: Schema, data: Data): ValidationResult<Data> {
    const { value, error } = schema.validate(data, {
        stripUnknown: true,
        abortEarly: false,
    });

    if (!error) {
        return { value, errors: null };
    }

    return {
        value,
        errors: error.details.map(({ message }) => ({ message })),
    };
}
