import 'jest-extended';
import { handler } from '../disburse';
import { Loan, LOAN_STATUS } from '../../domain';
import { ResponseCodes, sendToQueue } from '../../../core';
import { repository } from '../../repository';

jest.mock('../../repository', () => ({
    repository: {
        getById: jest.fn(() => null),
        updateLoanStatus: jest.fn(),
    },
}));

jest.mock('../../../core', () => {
    const actualExports = jest.requireActual('../../../core');

    return {
        ...actualExports,
        sendToQueue: jest.fn(),
    };
});

const loanMock: Loan = {
    id: 'abc',
    amount: 100,
    company: {},
    status: LOAN_STATUS.offered,
    isDeleted: false,
    updatedAt: '1649724546922',
    createdAt: '1649724546924',
};

describe('Loan disburse request', () => {
    const event = {
        pathParameters: {
            id: loanMock.id,
        },
    };

    it('throws not found error if it could not find the loan by the given id', async () => {
        const res = await handler(event as any);
        expect(res.statusCode).toEqual(ResponseCodes.notFound);

        const body = JSON.parse(res.body);
        expect(body).toMatchSnapshot();
    });

    it('throws not found error if the loan has marked as deleted', async () => {
        (repository.getById as jest.Mock).mockImplementationOnce(() => ({
            ...loanMock,
            isDeleted: true,
        }));

        const res = await handler(event as any);
        expect(res.statusCode).toEqual(ResponseCodes.notFound);

        const body = JSON.parse(res.body);
        expect(body).toMatchSnapshot();
    });

    it('throws unprocessable error if the loan has already been disbursed', async () => {
        (repository.getById as jest.Mock).mockImplementationOnce(() => ({
            ...loanMock,
            status: LOAN_STATUS.disbursed,
        }));

        const res = await handler(event as any);
        expect(res.statusCode).toEqual(ResponseCodes.unprocessable);

        const body = JSON.parse(res.body);
        expect(body).toMatchSnapshot();
    });

    it('throws unprocessable error if the loan is in process', async () => {
        (repository.getById as jest.Mock).mockImplementationOnce(() => ({
            ...loanMock,
            status: LOAN_STATUS.processing,
        }));

        const res = await handler(event as any);
        expect(res.statusCode).toEqual(ResponseCodes.unprocessable);

        const body = JSON.parse(res.body);
        expect(body).toMatchSnapshot();
    });

    it('accepts the request and returns the loan status', async () => {
        (repository.getById as jest.Mock).mockImplementationOnce(() => loanMock);

        const res = await handler(event as any);
        expect(res.statusCode).toEqual(ResponseCodes.accepted);

        expect(repository.updateLoanStatus).toHaveBeenLastCalledWith(
            loanMock.id,
            LOAN_STATUS.processing
        );

        expect(sendToQueue).toHaveBeenLastCalledWith(
            JSON.stringify({
                loanId: loanMock.id,
            })
        );

        const body = JSON.parse(res.body);
        expect(body).toEqual({
            loanId: loanMock.id,
            status: LOAN_STATUS.processing,
        });
    });
});
