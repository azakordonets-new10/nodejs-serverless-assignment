import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

import { BadRequestError, jsonParser, ResponseCodes, withErrorHandler } from '../../core';
import { applyLoanSchema, validate } from '../validator';
import { applyLoan } from '../service';

interface ApplyLoanRequestInputs {
    amount: number;
    companyId: string;
}

async function applyLoanHandler(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const data = jsonParser(event.body ?? '');
    const { value, errors } = validate<ApplyLoanRequestInputs>(applyLoanSchema, data);

    if (errors) {
        throw new BadRequestError('Invalid request params', { errors });
    }

    // Since there is no validation errors, we can safely access the value
    const { amount, companyId } = value as ApplyLoanRequestInputs;
    const loan = await applyLoan(amount, companyId);

    return {
        statusCode: ResponseCodes.created,
        body: JSON.stringify(loan),
    };
}

export const handler = withErrorHandler(applyLoanHandler);
