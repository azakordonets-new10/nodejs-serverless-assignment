import { repository } from './repository';
import { sendToQueue, getCompanyById, NotFoundError, UnprocessableError } from '../core';
import { getLoanData, hasLoanDisbursed, isLoanInProcess, Loan, LOAN_STATUS } from './domain';

export async function applyLoan(amount: number, companyId: string): Promise<Loan> {
    const companyData = await getCompanyById(companyId);

    if (!companyData.actief) {
        throw new UnprocessableError('Only an active company can apply for a loan', {
            companyId,
        });
    }

    const loan = getLoanData(amount, companyData);
    await repository.create(loan);

    // todo: return only necessary fields to the user (via data transfer object)
    return loan;
}

// this function performs a safe delete.
export async function markLoanAsDeleted(loanId: string): Promise<Loan | undefined> {
    const loan = await repository.getById(loanId);

    if (!loan || loan.isDeleted) {
        throw new NotFoundError('Loan not found', { loanId });
    }

    return repository.deleteLoanById(loanId);
}

export async function getAllLoans(): Promise<Loan[]> {
    // implement paging if needed
    return repository.getAllLoans();
}

export async function disburseLoan(
    loanId: string
): Promise<{ status: LOAN_STATUS.processing; loanId: string }> {
    const loan = await repository.getById(loanId);

    if (!loan || loan.isDeleted) {
        throw new NotFoundError('Loan not found', { loanId });
    }

    if (hasLoanDisbursed(loan.status)) {
        throw new UnprocessableError('The loan has already been disbursed', {
            loanStatus: loan.status,
        });
    }

    if (isLoanInProcess(loan.status)) {
        throw new UnprocessableError('The loan is in process', {
            loanStatus: loan.status,
        });
    }

    await repository.updateLoanStatus(loanId, LOAN_STATUS.processing);

    const loanDisbursementRequestPayload = JSON.stringify({ loanId });
    await sendToQueue(loanDisbursementRequestPayload);

    return {
        loanId,
        status: LOAN_STATUS.processing,
    };
}
