import { marshall, unmarshall } from '@aws-sdk/util-dynamodb';
import {
    PutItemCommand,
    PutItemCommandInput,
    GetItemCommand,
    GetItemCommandInput,
    UpdateItemCommand,
    UpdateItemCommandInput,
    ScanCommand,
    ScanCommandInput,
} from '@aws-sdk/client-dynamodb';

import { DBClient } from '../core/db-client';
import { Loan, LOAN_STATUS } from './domain';

const tableName = process.env.DYNAMODB_TABLE;

async function create(data: Loan) {
    const params: PutItemCommandInput = {
        TableName: tableName,
        Item: marshall(data),
    };

    const item = new PutItemCommand(params);
    await DBClient.send(item);
}

async function getById(loanId: string): Promise<Loan | undefined> {
    const params: GetItemCommandInput = {
        TableName: tableName,
        Key: marshall({ id: loanId }),
    };

    const result = await DBClient.send(new GetItemCommand(params));

    if (!result.Item) return;

    return unmarshall(result.Item) as Loan;
}

async function deleteLoanById(loanId: string): Promise<Loan | undefined> {
    const params: UpdateItemCommandInput = {
        TableName: tableName,
        Key: marshall({ id: loanId }),
        UpdateExpression: 'SET isDeleted = :d, updatedAt = :u',
        ExpressionAttributeValues: marshall({
            ':d': true,
            ':u': Date.now().toString(),
        }),
        ReturnValues: 'ALL_NEW',
    };

    const result = await DBClient.send(new UpdateItemCommand(params));

    // Updated item can be undefined
    if (!result.Attributes) return;

    return unmarshall(result.Attributes) as Loan;
}

async function getAllLoans(): Promise<Loan[]> {
    const params: ScanCommandInput = {
        TableName: tableName,
        FilterExpression: 'isDeleted = :d',
        ExpressionAttributeValues: marshall({
            ':d': false,
        }),
    };

    const result = await DBClient.send(new ScanCommand(params));

    if (!result.Items) return [];

    return result.Items.map((Item) => unmarshall(Item)) as Loan[];
}

async function updateLoanStatus(loanId: string, loanStatus: LOAN_STATUS) {
    const params: UpdateItemCommandInput = {
        TableName: tableName,
        Key: marshall({ id: loanId }),
        // `status` is a reserved keyword in Dynamodb.
        // So, can't directly use it in the update expression
        UpdateExpression: 'SET #statusKeyName = :s, updatedAt = :u',
        ExpressionAttributeNames: {
            '#statusKeyName': 'status',
        },
        ExpressionAttributeValues: marshall({
            ':s': loanStatus,
            ':u': Date.now().toString(),
        }),
        ReturnValues: 'ALL_NEW',
    };

    await DBClient.send(new UpdateItemCommand(params));
}

export const repository = {
    create,
    getById,
    deleteLoanById,
    getAllLoans,
    updateLoanStatus,
};
